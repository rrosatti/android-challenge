package rrosatti.androidchallenge.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rrosatti.androidchallenge.R;
import rrosatti.androidchallenge.adapters.PullRequestsAdapter;
import rrosatti.androidchallenge.interfaces.GitHubService;
import rrosatti.androidchallenge.model.PullRequest;
import rrosatti.androidchallenge.utils.ApiUtils;

public class PullRequestsActivity extends AppCompatActivity {

    private String authorName;
    private String repoName;
    private RecyclerView listPullRequest;
    private PullRequestsAdapter adapter;
    private GitHubService gitHubService;
    private List<PullRequest> pullRequests;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        authorName = getIntent().getStringExtra("authorName");
        repoName = getIntent().getStringExtra("repoName");

        if (authorName.isEmpty() || repoName.isEmpty()) {
            Toast.makeText(this, "Something went wrong ... ", Toast.LENGTH_SHORT).show();
            finish();
        }

        getSupportActionBar().setTitle(repoName);

        progressBar = (ProgressBar) findViewById(R.id.progressBarPullRequests);
        gitHubService = ApiUtils.getGitHubService();
        listPullRequest = (RecyclerView) findViewById(R.id.listPullRequests);
        adapter = new PullRequestsAdapter(this, new ArrayList<PullRequest>(0),
                new PullRequestsAdapter.PullRequestListener() {
            @Override
            public void onPullRequestClick(String url) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        listPullRequest.setLayoutManager(layoutManager);
        listPullRequest.setAdapter(adapter);
        listPullRequest.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        listPullRequest.addItemDecoration(itemDecoration);

        if (savedInstanceState != null) {
            progressBar.setVisibility(View.GONE);
            pullRequests = (List<PullRequest>) savedInstanceState.getSerializable("list");
            adapter.updatePullRequest(pullRequests);
        } else {
            loadPullRequests();
        }
    }

    public void loadPullRequests() {
        gitHubService.getPullRequests(authorName, repoName).enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    pullRequests = response.body();
                    adapter.updatePullRequest(pullRequests);
                    Log.d("PullRequestActivity", "Pull requests loaded from GitHub API.");
                } else {
                    // TODO: handle request errors
                }
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Something went wrong ...", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
                Log.e("PullRequestActivity", "Error loading from API.");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable("list", (Serializable) pullRequests);
    }
}
