package rrosatti.androidchallenge.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rrosatti.androidchallenge.R;
import rrosatti.androidchallenge.adapters.RepositoriesAdapter;
import rrosatti.androidchallenge.interfaces.GitHubService;
import rrosatti.androidchallenge.model.GitHubRepositoryResponse;
import rrosatti.androidchallenge.model.Repository;
import rrosatti.androidchallenge.utils.ApiUtils;

public class MainActivity extends AppCompatActivity {

    private RepositoriesAdapter adapter;
    private RecyclerView listRepositories;
    private GitHubService gitHubService;
    private ProgressBar progressBar;

    private boolean isLoading = false;
    private int currentPage = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gitHubService = ApiUtils.getGitHubService();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        listRepositories = (RecyclerView) findViewById(R.id.listRepositories);
        adapter = new RepositoriesAdapter(this, new ArrayList<Repository>(0),
                new RepositoriesAdapter.RepositoryListener() {
            @Override
            public void onRepositoryClick(String authorName, String repoName) {
                Intent in = new Intent(getApplicationContext(), PullRequestsActivity.class);
                in.putExtra("authorName", authorName);
                in.putExtra("repoName", repoName);
                startActivity(in);
            }
        });

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listRepositories.setLayoutManager(layoutManager);
        listRepositories.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        listRepositories.addItemDecoration(itemDecoration);

        if (savedInstanceState != null) {
            List<Repository> repositories = (List<Repository>) savedInstanceState.getSerializable("repositories");
            adapter.updateRepositories(repositories);
            this.currentPage = savedInstanceState.getInt("currentPage");
        }

        listRepositories.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                isLoading = true;
                int pastVisibleItems, visibleItemsCount, totalItemCount;

                if (dy > 0) {
                    visibleItemsCount = layoutManager.getChildCount();
                    pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                    totalItemCount = layoutManager.getItemCount();

                    if (isLoading) {
                        if ((visibleItemsCount + pastVisibleItems) >= totalItemCount) {
                            isLoading = false;
                            progressBar.setVisibility(View.VISIBLE);
                            loadRepositories(++currentPage);
                        }
                    }
                }
            }

        });

        listRepositories.setAdapter(adapter);

    }

    public void loadRepositories(int page) {
        gitHubService.getRepositories(page).enqueue(new Callback<GitHubRepositoryResponse>() {
            @Override
            public void onResponse(Call<GitHubRepositoryResponse> call, Response<GitHubRepositoryResponse> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    adapter.addAll(response.body().getRepositories());
                    //isLoading = false;
                    Log.d("MainActivity", "Repositories loaded from GitHub API.");
                } else {
                    // TODO: handle request errors
                }
            }

            @Override
            public void onFailure(Call<GitHubRepositoryResponse> call, Throwable t) {
                //showErrorMessage(t.getStackTrace());
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
                Log.e("MainActivity", "Error loading from API.");
            }

        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadRepositories(currentPage);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("repositories", (Serializable) adapter.getRepositories());
        outState.putInt("currentPage", currentPage);
    }
}
