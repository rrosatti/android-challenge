package rrosatti.androidchallenge.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rrosatti on 10/12/17.
 */

public class GitHubRepositoryResponse implements Serializable {

    @SerializedName("items")
    @Expose
    private List<Repository> repositories = null;
    @SerializedName("total_count")
    @Expose
    private Integer numRepositories;

    public List<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }

    public Integer getNumRepositories() {
        return numRepositories;
    }

    public void setNumRepositories(Integer numRepositories) {
        this.numRepositories = numRepositories;
    }
}
