package rrosatti.androidchallenge.utils;

import rrosatti.androidchallenge.interfaces.GitHubService;
import rrosatti.androidchallenge.remote.RetrofitClient;

/**
 * Created by rrosatti on 10/10/17.
 */

public class ApiUtils {

    public static final String BASE_URL = "https://api.github.com";

    public static GitHubService getGitHubService() {
        return RetrofitClient.getClient(BASE_URL).create(GitHubService.class);
    }

}
