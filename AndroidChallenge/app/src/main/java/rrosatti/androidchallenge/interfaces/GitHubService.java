package rrosatti.androidchallenge.interfaces;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rrosatti.androidchallenge.model.GitHubRepositoryResponse;
import rrosatti.androidchallenge.model.PullRequest;
import rrosatti.androidchallenge.model.User;

/**
 * Created by rrosatti on 10/10/17.
 */

public interface GitHubService {

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<GitHubRepositoryResponse> getRepositories(@Query("page") Integer page);

    @GET("repos/{user}/{repo}/pulls")
    Call<List<PullRequest>> getPullRequests(@Path("user") String userName, @Path("repo") String repoName);

    @GET("users/{user}")
    Call<User> getUser(@Path("user") String username);

}
