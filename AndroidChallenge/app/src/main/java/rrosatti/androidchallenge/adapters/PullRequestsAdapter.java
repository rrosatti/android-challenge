package rrosatti.androidchallenge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rrosatti.androidchallenge.R;
import rrosatti.androidchallenge.interfaces.GitHubService;
import rrosatti.androidchallenge.model.PullRequest;
import rrosatti.androidchallenge.model.User;
import rrosatti.androidchallenge.utils.ApiUtils;

/**
 * Created by rrosatti on 10/12/17.
 */

public class PullRequestsAdapter extends RecyclerView.Adapter<PullRequestsAdapter.ViewHolder> {

    private List<PullRequest> pullRequests;
    private Context context;
    private PullRequestListener pullRequestListener;
    private GitHubService gitHubService;


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView txtPullRequestTitle;
        public TextView txtPullRequestBody;
        public ImageView imgAuthor;
        public TextView txtAuthorUsername;
        PullRequestListener pullRequestListener;

        public ViewHolder(View itemView, PullRequestListener pullRequestListener) {
            super(itemView);
            txtPullRequestTitle = itemView.findViewById(R.id.txtPullRequestTitle);
            txtPullRequestBody = itemView.findViewById(R.id.txtPullRequestBody);
            imgAuthor = itemView.findViewById(R.id.pullRequestImgAuthor);
            txtAuthorUsername = itemView.findViewById(R.id.pullRequestTxtAuthorUsername);

            this.pullRequestListener = pullRequestListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            PullRequest pullRequest = getPullRequest(getAdapterPosition());
            this.pullRequestListener.onPullRequestClick(pullRequest.getPullRequestUrl());
            notifyDataSetChanged();
        }

    }

    public PullRequestsAdapter(Context context, List<PullRequest> pullRequests,
                               PullRequestListener pullRequestListener) {
        this.context = context;
        this.pullRequests = pullRequests;
        this.pullRequestListener = pullRequestListener;
    }

    @Override
    public PullRequestsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View pullRequestView = inflater.inflate(R.layout.pull_request_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(pullRequestView, this.pullRequestListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PullRequest pullRequest = pullRequests.get(position);
        TextView txtPullRequestTitle = holder.txtPullRequestTitle;
        TextView txtPullRequestBody = holder.txtPullRequestBody;
        ImageView imgAuthor = holder.imgAuthor;
        TextView txtAuthorUsername = holder.txtAuthorUsername;

        txtPullRequestTitle.setText(pullRequest.getTitle());
        txtPullRequestBody.setText(pullRequest.getBody());
        txtAuthorUsername.setText(pullRequest.getUser().getUsername());

        Picasso.with(context)
                .load(pullRequest.getUser().getPhoto())
                .resize(160, 160)
                .into(imgAuthor);
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    public void updatePullRequest(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
        notifyDataSetChanged();
    }

    public PullRequest getPullRequest(int adapterPosition) {
        return pullRequests.get(adapterPosition);
    }

    public interface PullRequestListener {
        void onPullRequestClick(String url);
    }
}
