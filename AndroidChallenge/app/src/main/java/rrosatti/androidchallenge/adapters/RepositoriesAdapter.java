package rrosatti.androidchallenge.adapters;

import android.content.ClipData;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rrosatti.androidchallenge.R;
import rrosatti.androidchallenge.interfaces.GitHubService;
import rrosatti.androidchallenge.model.Repository;
import rrosatti.androidchallenge.model.User;
import rrosatti.androidchallenge.utils.ApiUtils;

/**
 * Created by rrosatti on 10/12/17.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.ViewHolder> {

    private List<Repository> repositories;
    private Context context;
    private RepositoryListener repositoryListener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txtRepoName;
        public TextView txtRepoDescription;
        public TextView txtForksCount;
        public TextView txtStarsCount;
        public TextView txtAuthorUsername;
        public ImageView imgAuthorPicture;
        RepositoryListener repositoryListener;

        public ViewHolder(View itemView, RepositoryListener repositoryListener) {
            super(itemView);
            txtRepoName = itemView.findViewById(R.id.txtRepoName);
            txtRepoDescription = itemView.findViewById(R.id.txtRepoDescription);
            txtForksCount = itemView.findViewById(R.id.txtForksCount);
            txtStarsCount = itemView.findViewById(R.id.txtStarsCount);
            txtAuthorUsername = itemView.findViewById(R.id.txtAuthorUsername);
            imgAuthorPicture = itemView.findViewById(R.id.imgAuthor);

            this.repositoryListener = repositoryListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Repository repository = getRepository(getAdapterPosition());
            this.repositoryListener.onRepositoryClick(repository.getOwner().getUsername(), repository.getName());

            notifyDataSetChanged();
        }
    }

    public RepositoriesAdapter(Context context, List<Repository> repositories,
                               RepositoryListener repositoryListener) {
        this.repositories = repositories;
        this.context = context;
        this.repositoryListener = repositoryListener;
    }

    @Override
    public RepositoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View repositoryView = inflater.inflate(R.layout.repository_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(repositoryView, this.repositoryListener);
        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(RepositoriesAdapter.ViewHolder holder, int position) {
        final Repository repository = repositories.get(position);
        TextView txtRepoName = holder.txtRepoName;
        TextView txtRepoDescription = holder.txtRepoDescription;
        TextView txtForksCount = holder.txtForksCount;
        TextView txtStarsCount = holder.txtStarsCount;
        TextView txtAuthorUsername = holder.txtAuthorUsername;
        ImageView imgAuthorPicture = holder.imgAuthorPicture;

        txtRepoName.setText(repository.getName());
        txtRepoDescription.setText(repository.getDescription());
        txtForksCount.setText(String.valueOf(repository.getNumForks()));
        txtStarsCount.setText(String.valueOf(repository.getStarsCount()));
        txtAuthorUsername.setText(repository.getOwner().getUsername());

        Picasso.with(context)
                .load(repository.getOwner().getPhoto())
                .resize(160, 160)
                .into(imgAuthorPicture);
    }

    @Override
    public int getItemCount() {
        return repositories == null ? 0 : repositories.size();
    }

    public void updateRepositories(List<Repository> repositories) {
        this.repositories = repositories;
        notifyDataSetChanged();
    }

    public Repository getRepository(int adapterPosition) {
        return repositories.get(adapterPosition);
    }

    public interface RepositoryListener {
        void onRepositoryClick(String authorName, String repoName);
    }

    public void add(Repository repo) {
        repositories.add(repo);
        notifyItemInserted(repositories.size() - 1);
    }

    public void addAll(List<Repository> repositories) {
        for (Repository repo : repositories) {
            add(repo);
        }
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

}
